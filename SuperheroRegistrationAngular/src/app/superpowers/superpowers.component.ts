import { Component } from '@angular/core';
import { Superhero } from '../models/superhero.model';
import { Superpower } from '../models/superpower.model';
import { SuperheroService } from '../providers/superhero.service';
import { SuperpowerService } from '../providers/superpower.service';
import { CategoryService } from '../providers/category.service';
import { Category } from '../models/category.model';
import { Location } from '@angular/common';
import { bufferToggle } from 'rxjs';

@Component({
  selector: 'app-superpowers',
  templateUrl: './superpowers.component.html',
  styleUrls: ['./superpowers.component.css']
})
export class SuperpowersComponent {
  superheroes!: Array<Superhero>;
  superpowers!: Array<Superpower>;
  powerCategories!: Array<Category>;
  weaknesses!: Array<Category>;
  // deleteData!: any;
  noMatchingPowersBool: boolean = false;
  noMatchingWeaknessesBool: boolean = false;
  powerCatg!: Category;

  constructor(private superheroService: SuperheroService,
    private superpowerService: SuperpowerService,
    private categoryService: CategoryService,
    private location: Location) {
  }

  ngOnInit(): void {
    this.getAllCategories();
    this.getAllSuperpowers();
  }

  getAllSuperpowers() {
    this.noMatchingPowersBool = false;
    this.superpowerService.getAllSuperpowers().subscribe(data => {
      this.superpowers = data;
      if (this.superpowers.length == 0 || this.superpowers == undefined) {
        this.noMatchingPowersBool = true;
      }
    });
  }

  onSelectCategory(event: any) {

    const selectionVal = event.target.value;

    if (selectionVal == "") {
      this.getAllSuperpowers();
      this.noMatchingPowersBool = false;
    }

    this.categoryService.getCategoryById(selectionVal).subscribe(data => {
      if (!!data.superpowers) {
        this.superpowers = data.superpowers;
        this.superpowers.forEach(power => {
          power.catgName = data.name;
          power.catgDescription = data.description;
        });
      }

      if (selectionVal != "" && this.superpowers.length > 0) {
        this.noMatchingPowersBool = false;
      } else if (selectionVal != "" || this.superpowers.length == 0) {
        this.noMatchingPowersBool = true;
      }

    });
  }

  getAllCategories() {
    this.categoryService.getAllCategories().subscribe(data => {
      this.powerCategories = data.filter(c => c.isWeakness == false || c.isWeakness == null);
      this.weaknesses = data.filter(c => c.isWeakness == true);
      this.getAllWeaknesses();
    });
  }

  getAllWeaknesses() {
    this.categoryService.getCategoriesByWeakness().subscribe(data => {
      this.weaknesses = data;
      if (this.weaknesses.length > 0) {
        this.noMatchingWeaknessesBool = false;
      } else {
        this.noMatchingWeaknessesBool = true;
      }
    });
  }

  searchByPowerName(name: string) {
    this.superpowerService.getSuperpowerByName(name).subscribe(data => {
      this.superpowers = data;
      if (this.superpowers.length > 0) {
        this.noMatchingPowersBool = false;
      } else {
        this.noMatchingPowersBool = true;
      }
    });
  }

  searchByWeaknessName(name: string) {
    this.categoryService.getCategoriesByWeakness().subscribe(data => {
      this.weaknesses = data.filter(c => c.name.toLowerCase().includes(name.toLowerCase()));
      if (this.weaknesses.length > 0) {
        this.noMatchingWeaknessesBool = false;
      } else {
        this.noMatchingWeaknessesBool = true;
      }
    });
  }

  goBack(): void {
    this.location.back();
  }

  getColor(category: string): string {
    var color: string = "white";
    switch (category) {
      case 'Physical':
        color = '#67c7eb'; //bright blue
        break;
      case 'Mental': //teal
        color = '#12b296';
        break;
      case 'Elemental': //bright green
        color = '#7FFF00';
        break;
      case 'Technological': //red
        color = '#ce0000';
        break;
      case 'Psychic': // bright purple
        color = '#e92efb';
        break;
      case 'Energy': // bright yellow
        color = '#fcf340';
        break;
      case 'Transformation': // bright pink
        color = '#ff2079';
        break;
      case 'Time': //orange
        color = '#ffaa01';
        break;
      default:
        return 'white';
    }
    return color;
  }

}
