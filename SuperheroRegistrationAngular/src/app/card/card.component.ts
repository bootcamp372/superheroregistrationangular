import { Component, Input } from '@angular/core';
import { Superhero } from '../models/superhero.model';
import { SuperheroService } from '../providers/superhero.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})

export class CardComponent {
  @Input()
  superhero!: Superhero;

  @Input()
  parent!: string;

  constructor(private superheroService: SuperheroService, private router: Router) {
  }

  openConfirmationDialog(superhero: Superhero) {
    if (confirm("Are you sure you want to delete " + superhero.name + "?")) {
      console.log("User confirmed deletion");
      this.deleteSuperhero(superhero.superheroId);
    }
  }

  deleteSuperhero(superheroId: number) {
    this.superheroService.deleteSuperhero(superheroId).subscribe(data => {
      alert("Superhero with ID " + superheroId + " has been deleted.");
      this.reloadCurrentRoute();
    });
  }

  reloadCurrentRoute() {
    let currentUrl = this.router.url;
    this.router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
        this.router.navigate([currentUrl]);
    });
  }

}
