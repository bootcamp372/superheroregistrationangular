import { Component } from '@angular/core';
import { Superhero } from '../models/superhero.model';
import { Superpower } from '../models/superpower.model';
import { SuperheroService } from '../providers/superhero.service';
import { SuperpowerService } from '../providers/superpower.service';
import { Category } from '../models/category.model';
import { ActivatedRoute, Params } from '@angular/router';
import { formatDate } from '@angular/common';
import { Location } from '@angular/common';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
@Component({
  selector: 'app-superhero-form',
  templateUrl: './superhero-form.component.html',
  styleUrls: ['./superhero-form.component.css']
})
export class SuperheroFormComponent {
  title: string = 'Register Superhero';
  superhero!: Superhero;
  superheroId!: string;
  showModalBool: boolean = false;
  action!: string;
  displayStyle: string = "none";
  modalTitle: string = "Registration Form Submitted";
  modalBody: string = "Thank you for registering. A representative from the agency will be in contact.";

  selectedPowers?: Superpower[] = new Array<Superpower>();
  newHeroPowers: Superpower[] = new Array<Superpower>();

  allPowers: Array<Superpower> = [];
  maxDob: string = new Date().toISOString().split('T')[0];
  newSuperhero: Superhero = new Superhero(0, "", "", "", "", "", "", "", "", false, "", "");



  isValid: Boolean = false;

  constructor(private activatedRoute: ActivatedRoute,
    private superheroService: SuperheroService,
    private superPowerService: SuperpowerService,
    private location: Location,
    private fb: FormBuilder
  ) {
  }

  ngOnInit() {

    this.activatedRoute.params
      .subscribe(
        (params: Params) => {

          let id = params['id'];
          if (id != undefined) {
            this.superheroId = params['id'];
            this.title = "Update Superhero";
            this.modalTitle = "Update Superhero";
            this.modalBody = "Superhero data has been updated!";
            this.action = "PUT";

            this.superheroService.getSuperheroById(id).subscribe(data => {
              this.newSuperhero.name = data.name;
              this.newSuperhero.nickname = data.nickname;
              this.newSuperhero.alias = data.alias;
              this.newSuperhero.description = data.description;
              this.newSuperhero.email = data.email;
              this.newSuperhero.imagePath = data.imagePath;
              this.newSuperhero.phoneNumber = data.phoneNumber;
              this.newSuperhero.featured = data.featured;
              this.newSuperhero.superheroId = data.superheroId;
              this.newSuperhero.dob = formatDate(data.dob, 'yyyy-MM-dd', 'en-US');
            });
          } else {
            this.title = "Register New Superhero";
            this.modalTitle = "Registration Form Submitted";
            this.modalBody = "Thank you for registering. A representative from the agency will be in contact.";
            this.action = "POST";
            this.newSuperhero.featured = false;
          }

          this.superPowerService.getAllSuperpowers().subscribe(data => {
            this.allPowers = data;
          });

        }
      );
  }


  openPopup() {
    this.displayStyle = "block";
  }

  onClose(): void {
    this.displayStyle = "none";
  }

  onSubmitForm() {
    this.isValid = this.validateInputs();

    if (this.isValid) {
      if (this.action == "PUT") {
        this.modalTitle = "Update Superhero";
        this.modalBody = "Superhero data has been updated!";
        this.superheroService.updateSuperhero(this.newSuperhero, this.superheroId).subscribe(data => {
          this.openPopup();
          this.showModalBool = true;
        },
          error => {
            this.showModalBool = false;
            console.log(error.status); // the HTTP status code
            console.log(error.status); // the status code message
            console.log(error.error); // the message from the API
          });
      } else {
        this.modalTitle = "Registration Form Submitted";
        this.modalBody = "Thank you for registering. A representative from the agency will be in contact.";
        this.superheroService.createSuperhero(this.newSuperhero).subscribe(data => {
          this.openPopup();
          this.showModalBool = true;
        },
          error => {
            this.showModalBool = false;
            console.log(error.status); // the HTTP status code
            console.log(error.status); // the status code message
            console.log(error.error); // the message from the API
          });
      }
    } else if (!this.isValid) {
      this.modalTitle = "Error Validating Form";
      this.modalBody = "Please fill out the required fields and correct any validation errors.";
      this.openPopup();
      this.showModalBool = true;
      event?.preventDefault();
      event?.stopPropagation();
    }
  }

  goBack(): void {
    this.location.back();
  }

  validateInputs() {
    let inputs, index;
    let isValid = true;

    inputs = document.getElementsByTagName('input');

    for (index = 0; index < inputs.length; ++index) {
        var currentInput = inputs[index];
        if (!currentInput.checkValidity()) {
            isValid = false;
            currentInput.setAttribute('aria-invalid', String(!isValid));
            const errorElement = document.createElement('span');
            errorElement.id = `${currentInput.id}-error`;
            currentInput.setAttribute('aria-describedby', errorElement.id);
            currentInput.insertAdjacentElement('afterbegin',errorElement);
            return isValid;
        }
    }
    return isValid;
  }

}
