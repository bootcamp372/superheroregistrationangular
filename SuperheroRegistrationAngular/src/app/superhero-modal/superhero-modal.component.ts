import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-superhero-modal',
  templateUrl: './superhero-modal.component.html',
  styleUrls: ['./superhero-modal.component.css']
})
export class SuperheroModalComponent {
  displayStyle!: string;
  @Input() title!: string;
  @Input() body!:string;

  @Output() close: EventEmitter<void> = new EventEmitter<void>();

  onModalClose(): void {
    this.close.emit();
  }

}
