import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, Subject, of} from 'rxjs';
import { map } from 'rxjs/operators';
import { Superhero } from '../models/superhero.model';
import {Superpower} from '../models/superpower.model';

@Injectable({
  providedIn: 'root'
})
export class SuperpowerService {

  constructor(private http: HttpClient) { }

  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  private superpowerURL: string =
  'https://localhost:7262/api/Superpowers';

  getAllSuperpowers(): Observable<Superpower[]> {
    return this.http.get(this.superpowerURL,
      this.httpOptions).pipe(map(res => <Superpower[]>res));
  }

  getSuperpowerByName(name: string): Observable<Superpower[]>
  {
    return this.http.get((this.superpowerURL + "/search?name=" + name), this.httpOptions)
    .pipe( map (res=> <Superpower[]>res))
    ;
  }

}
