
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, Subject, of} from 'rxjs';
import { map } from 'rxjs/operators';
import { Superhero } from '../models/superhero.model';
import {Superpower} from '../models/superpower.model';
@Injectable({
  providedIn: 'root'
})
export class SuperheroService {

  constructor(private http: HttpClient) { }

  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  private superheroURL: string =
  'https://localhost:7262/api/Superheroes';

  getAllSuperheroes(): Observable<Superhero[]> {
    return this.http.get(this.superheroURL,
      this.httpOptions).pipe(map(res => <Superhero[]>res));
  }

  getSuperheroById(id: string): Observable<Superhero> {
    return this.http.get(this.superheroURL + '/' + id,
      this.httpOptions).pipe(map(res => <Superhero>res));
  }

  createSuperhero(superhero: Superhero): Observable<Superhero> {
    return this.http.post<Superhero>(this.superheroURL, superhero, this.httpOptions)
          .pipe(map(res => <Superhero>res));
  }

  updateSuperhero(superhero: Superhero, id: string): Observable<Superhero> {
    return this.http.put<Superhero>(this.superheroURL + '/' + id, superhero, this.httpOptions)
          .pipe(map(res => <Superhero>res));
  }

  deleteSuperhero(id: number): Observable<Superhero> {
    return this.http.delete(this.superheroURL + '/' + id,
      this.httpOptions).pipe(map(res => <Superhero>res));
  }

  getSuperheroesByName(name: string): Observable<Superhero[]>
  {
    return this.http.get((this.superheroURL + "/search?name=" + name), this.httpOptions)
    .pipe( map (res=> <Superhero[]>res))
    ;
  }

  getSuperheroesByPower(power: string): Observable<Superhero[]>
  {
    return this.http.get((this.superheroURL + "/search-power?superpower=" + power), this.httpOptions)
    .pipe( map (res=> <Superhero[]>res))
    ;
  }

}
