import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, Subject, of} from 'rxjs';
import { map } from 'rxjs/operators';
import { Superhero } from '../models/superhero.model';
import { Category } from '../models/category.model';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  constructor(private http: HttpClient) { }

  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  private categoriesURL: string =
  'https://localhost:7262/api/Categories';

  getAllCategories(): Observable<Category[]> {
    return this.http.get(this.categoriesURL,
      this.httpOptions).pipe(map(res => <Category[]>res));
  }

  getCategoryById(id: string): Observable<Category> {
    return this.http.get(this.categoriesURL + '/' + id,
      this.httpOptions).pipe(map(res => <Category>res));
  }

  getCategoriesByPower(): Observable<Category[]>
  {
    return this.http.get((this.categoriesURL + "/powers"), this.httpOptions)
    .pipe( map (res=> <Category[]>res))
    ;
  }

  getCategoriesByWeakness(): Observable<Category[]>
  {
    return this.http.get((this.categoriesURL + "/weaknesses"), this.httpOptions)
    .pipe( map (res=> <Category[]>res))
    ;
  }

  getCategoriesByName(name: string): Observable<Category[]>
  {
    return this.http.get((this.categoriesURL + "/search?name=" + name), this.httpOptions)
    .pipe( map (res=> <Category[]>res))
    ;
  }

}
