import { Component, Input } from '@angular/core';
import { Superhero } from '../models/superhero.model';
import { Superpower } from '../models/superpower.model';
import { SuperheroService } from '../providers/superhero.service';
import { SuperpowerService } from '../providers/superpower.service';
import { Category } from '../models/category.model';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {

  superheroes!: Array<Superhero>;
  featuredHeroes: Array<Superhero> = [];

  ngOnInit(): void {
    this.getAllSuperheroes();
  }

  constructor(private superheroService: SuperheroService, private superpowerService: SuperpowerService) {
  }

  getAllSuperheroes() {
    this.superheroService.getAllSuperheroes().subscribe(data => {
      this.superheroes = data;
      this.superheroes.forEach(hero => {

        if (hero.featured == true) {
          this.featuredHeroes.push(hero);
        }
      });
    });
  }

}

