import { Component } from '@angular/core';
import { Superhero } from '../models/superhero.model';
import { Superpower } from '../models/superpower.model';
import { SuperheroService } from '../providers/superhero.service';
import { SuperpowerService } from '../providers/superpower.service';
import { Category } from '../models/category.model';
import { Location } from '@angular/common';
@Component({
  selector: 'app-superheroes',
  templateUrl: './superheroes.component.html',
  styleUrls: ['./superheroes.component.css']
})
export class SuperheroesComponent {
  superheroes!: Array<Superhero>;
  superpowers!: Array<Superpower>;
  categories!: Array<Category>;
  deleteData!: any;
  noMatchingHeroesBool: boolean = false;
  // toggleModalBool: boolean = false;

  constructor(private superheroService: SuperheroService,
    private superpowerService: SuperpowerService,
    private location: Location) {
  }

  ngOnInit(): void {
    this.getAllSuperheroes();
    this.getAllSuperpowers();
  }

  getAllSuperheroes() {
    this.superheroService.getAllSuperheroes().subscribe(data => {
      this.superheroes = data;
      if (this.superheroes.length > 0) {
        this.noMatchingHeroesBool = false;
      } else {
        this.noMatchingHeroesBool = true;
      }
    });
  }

  getAllSuperpowers() {
    this.superpowerService.getAllSuperpowers().subscribe(data => {
      this.superpowers = data;
    });
  }

  onSelectSuperpower(event: any) {

    const selectedSuperpower = event.target.value;

    if (selectedSuperpower == "") {
      this.getAllSuperheroes();
      this.noMatchingHeroesBool = false;
    }

    this.superheroService.getSuperheroesByPower(selectedSuperpower).subscribe(data => {
      this.superheroes = data;
      if (selectedSuperpower != "" && this.superheroes.length > 0) {
        this.noMatchingHeroesBool = false;
      } else if (selectedSuperpower != "") {
        this.noMatchingHeroesBool = true;
      }
    });
  }

  searchByName(name: string) {
    this.superheroService.getSuperheroesByName(name).subscribe(data => {
      this.superheroes = data;
      if (this.superheroes.length > 0) {
        this.noMatchingHeroesBool = false;
      } else {
        this.noMatchingHeroesBool = true;
      }
    });
  }

  searchByPower(power: string) {

    this.superheroService.getSuperheroesByPower(power).subscribe(data => {
      this.superheroes = data;
      if (this.superheroes.length > 0) {
        this.noMatchingHeroesBool = false;
      } else {
        this.noMatchingHeroesBool = true;
      }
    });
  }

  goBack(): void {
    this.location.back();
  }
}




