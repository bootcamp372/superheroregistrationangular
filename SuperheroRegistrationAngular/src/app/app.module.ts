import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { SuperheroesComponent } from './superheroes/superheroes.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { CardComponent } from './card/card.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { switchMap } from 'rxjs/operators';

import { SuperheroDetailComponent } from './superhero-detail/superhero-detail.component';
import { SuperheroModalComponent } from './superhero-modal/superhero-modal.component';
import { FooterComponent } from './footer/footer.component';
import { SuperheroFormComponent } from './superhero-form/superhero-form.component';
import { SuperpowersComponent } from './superpowers/superpowers.component';

const appRoutes: Routes = [
  { path: "home", component: HomeComponent },
  { path: '',   redirectTo: '/home', pathMatch: 'full' },
  { path: "superheroes", component: SuperheroesComponent },
  { path: 'superheroes/:id', component: SuperheroDetailComponent },
  { path: "superhero-form", component: SuperheroFormComponent },
  { path: "superhero-form/:id", component: SuperheroFormComponent },
  { path: "superpowers", component: SuperpowersComponent },
  { path: "**", component: PageNotFoundComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SuperheroesComponent,
    NavBarComponent,
    CardComponent,
    PageNotFoundComponent,
    SuperheroDetailComponent,
    SuperheroModalComponent,
    FooterComponent,
    SuperheroFormComponent,
    SuperpowersComponent

  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
