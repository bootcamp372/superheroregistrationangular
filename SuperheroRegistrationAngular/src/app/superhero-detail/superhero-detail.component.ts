import { Component } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Router } from '@angular/router';
import { SuperheroService } from '../providers/superhero.service';
import { Superhero } from '../models/superhero.model';
import { Superpower } from '../models/superpower.model';
import { Location } from '@angular/common';
import { formatDate } from '@angular/common';

@Component({
  selector: 'app-superhero-detail',
  templateUrl: './superhero-detail.component.html',
  styleUrls: ['./superhero-detail.component.css']
})
export class SuperheroDetailComponent {
  superhero!: Superhero;
  superheroId!: string;
  deleteData!: any;
  toggleModalBool: boolean = false;

  constructor(private superheroService: SuperheroService,
    private route: ActivatedRoute,
    private router: Router,
    private location: Location) {
  }

  ngOnInit(): void {
    this.route.params
      .subscribe(
        (params: Params) => {
          this.superheroId = params['id'];
        }
      );
    this.superheroService.getSuperheroById(this.superheroId).subscribe(data => {
      this.superhero = data;
      this.superhero.dob = formatDate(this.superhero.dob, 'yyyy-MM-dd', 'en-US');
    });
  }

  goBack(): void {
    this.location.back();
  }

  addSuperhero(superhero: Superhero) {
    this.router.navigate(['superhero-form']);
  }

  editSuperhero(superhero: Superhero) {
    this.router.navigate(['superhero-form/' + this.superheroId]);
  }

  openConfirmationDialog(superhero: Superhero) {
    if (confirm("Are you sure you want to delete " + superhero.name + "?")) {
      console.log("User confirmed deletion");
      this.deleteSuperhero(superhero.superheroId);
    }
  }

  deleteSuperhero(superheroId: number) {
    this.superheroService.deleteSuperhero(superheroId).subscribe(data => {
      this.deleteData = data;
      alert("Superhero with ID " + superheroId + " has been deleted.");
      this.router.navigate(['superheroes']);
    });
  }

  getColor(category: string): string {
    var color: string = "white";
    switch (category) {
      case 'Physical':
        color = '#67c7eb'; //bright blue
        break;
      case 'Mental': //teal
        color = '#12b296';
        break;
      case 'Elemental': //bright green
        color = '#7FFF00';
        break;
      case 'Technological': //red
        color = '#ce0000';
        break;
      case 'Psychic': // bright purple
        color = '#e92efb';
        break;
      case 'Energy': // bright yellow
        color = '#fcf340';
        break;
      case 'Transformation': // bright pink
        color = '#ff2079';
        break;
      case 'Time': //orange
        color = '#ffaa01';
        break;
      default:
        return 'white';
    }
    return color;
  }

}
