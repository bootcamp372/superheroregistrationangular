import { Superpower } from "./superpower.model";
import { Superhero } from "./superhero.model";
export class Category {

  constructor(
    public catgId: number,
    public name: string,
    public description: string,
    public isWeakness: Boolean,
    public superpowers: Array<Superpower>,
    public superheroes?: Array<Superhero>) {
  }

}
