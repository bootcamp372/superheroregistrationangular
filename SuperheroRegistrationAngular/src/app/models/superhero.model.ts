import { Category } from "./category.model";
import { Superpower } from "./superpower.model";
export class Superhero {

  constructor(
    public superheroId: number,
    public name: string,
    public nickname: string,
    public description: string,
    public phoneNumber: string,
    public email: string,
    public dob: string,
    public alias: string,
    public imagePath: string,
    public featured: boolean,
    public weaknessName: string,
    public weaknessDescription: string,
    public superpowers?: Array<Superpower>
  ) { }
}
