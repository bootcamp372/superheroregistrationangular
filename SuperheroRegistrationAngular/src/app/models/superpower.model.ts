import { Category } from "./category.model";
import {Superhero} from "./superhero.model";
export class Superpower {
  constructor(
    public superpowerId: number,
    public name: string,
    public description: string,
    public catgName: string,
    public catgDescription: string,
    public category?: Category,
    public superheroes?: Array<Superhero>
  ) { }
}
